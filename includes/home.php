<div id="infinite-instagram-header">
    <img src="<?= INFINITE_INSTAGRAM_IMAGES ?>/logo-no-back.png" width="80px"/> Infinite Instagram
</div>
<div class="postbox-container" style="margin-top:30px; margin-right:30px; min-width: 450px;">
    <div class="postbox" style="text-align:center; min-width: 450px;">
        <h2 class="hndle">
            <span style="padding:10px;">Infinite Instagram Updates</span>
        </h2>
        <div class="inside updateNews" style="text-align:left;">
            <table class="widefat update-items"></table>
        </div>
    </div>
</div>

<div class="postbox-container" style="margin-top:30px; min-width: 450px;">
    <div class="postbox" style="text-align:center; min-width: 450px;">
        <h2 class="hndle">
            <span style="padding:10px;">Infinite Instagram News</span>
        </h2>
        <div class="inside news" style="text-align:left;">
            <table class="widefat news-items">

            </table>
        </div>
    </div>
</div>
