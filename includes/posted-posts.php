<div id="infinite-instagram-header">
    <img src="<?= INFINITE_INSTAGRAM_IMAGES ?>/logo-no-back.png" width="80px"/> Infinite Instagram posted posts
</div>
<table class="widefat fixed" cellspacing="0">
    <thead>
    <tr>
        <th id="columnname" class="manage-column column-columnname" scope="col">Image</th>
        <th id="columnname" class="manage-column column-columnname" scope="col">Caption text</th>
        <th id="columnname" class="manage-column column-columnname" scope="col">Posted at</th>
    </tr>
    </thead>
    <?php
    $postList = getListPostedPosts();
    foreach ($postList as $post) { ?>
        <tr>
            <td>
                <div class="thumbnail">
                    <?= '<img class="instagramImg" src="' . wp_upload_dir()['baseurl'] . '/instagram-uploads/' . $post->image . '" />'; ?>
                </div>
            </td>
            <td><?= $post->caption; ?></td>
            <td><?= $post->time; ?></td>
        </tr>
    <?php } ?>
</table>