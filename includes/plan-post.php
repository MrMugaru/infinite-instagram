<?php
uploadAndPlanPost();
?>
<div id="infinite-instagram-header">
    <img src="<?= INFINITE_INSTAGRAM_IMAGES ?>/logo-no-back.png" width="80px"/> Infinite Instagram plan post
</div>
<div id="wrapper">
    <div id="uploadForm">
        <h1>Plan a new Instagram post</h1>
        <form method="post" enctype="multipart/form-data">
            <table>
                <tr>
                    <td>Photo file:</td>
                    <td>
                        <input type='file' id='uploadInstagramImage' name='uploadInstagramImage'/>
                    </td>
                </tr>
                <tr>
                    <td>Caption Text:</td>
                    <td>
                        <textarea width="100%" rows="7" class="caption" cols="18" name='caption'></textarea>
                        <br>
                        Hashtag count:
                        <span class="hashtagCount">0</span>
                        <span class="hashtagCountStd">/30</span>
                    </td>
                </tr>
                <tr>
                    <td>Post date and time:</td>
                    <td>
                        <input type='datetime-local' class='txtPostTime' name='posttime'/>
                    </td>
                </tr>
                <tr class="errorMessage">
                    <td colspan="2">
                        testtest
                    </td>
                </tr>
                <tr class="successMessage">
                    <td colspan="2">
                        testtest
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <?php
                        submit_button('Plan this image for Instagram', 'primary btnPlanPost', 'submit', true)
                        ?>
                    </td>
                </tr>
            </table>
        </form>
    </div>
    <div id="imageForm">
        <h1>Picture Preview</h1>
        <img id="instagramImage" src="" alt="" width="400"/>
    </div>
</div>
