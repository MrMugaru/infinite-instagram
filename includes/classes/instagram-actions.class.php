<?php

function wpse_183245_upload_dir($dirs)
{
    $dirs['subdir'] = '/instagram-uploads';
    $dirs['path'] = $dirs['basedir'] . '/instagram-uploads';
    $dirs['url'] = $dirs['baseurl'] . '/instagram-uploads';
    return $dirs;
}

function uploadAndPlanPost()
{
    global $wpdb;
    // First check if the file appears on the _FILES array
    if (isset($_FILES['uploadInstagramImage'])) {
        add_filter('upload_dir', 'wpse_183245_upload_dir');
        // 0 means the content is not associated with any other posts
        $uploaded = media_handle_upload('uploadInstagramImage', 0);
        // Error checking using WP functions

        if (is_wp_error($uploaded)) {
            echo "Error uploading file: " . $uploaded->get_error_message();
        } else {
            $posttime = date("Y-m-d H:i:s", strtotime($_POST['posttime']));
            $uploadedFileUrl = basename(get_attached_file($uploaded));

            $insertPost = "INSERT INTO `" . $wpdb->prefix . "infinite_instagram_planned_posts` 
                      (`image`, `caption`, `time`, `added`) VALUES ('" . $uploadedFileUrl . "','" . $_POST['caption'] . "', '" . $posttime . "', NOW())";
            $wpdb->query($insertPost);

            echo "Post succesfully planned";

            remove_filter('upload_dir', 'wpse_183245_upload_dir');
        }
    }
}

function deletePlannedPost()
{

}

function getListPlannedPosts()
{
    global $wpdb;

    $getPlannedPosts = "SELECT * FROM `" . $wpdb->prefix . "infinite_instagram_planned_posts` WHERE `isPosted` = '0' ORDER BY `time` ASC";
    $resultPlannedPosts = $wpdb->get_results($getPlannedPosts);

    return $resultPlannedPosts;
}

function getListPostedPosts()
{
    global $wpdb;

    $getPostedPosts = "SELECT * FROM `" . $wpdb->prefix . "infinite_instagram_planned_posts` WHERE `isPosted` = '1' ORDER BY `time` DESC LIMIT 0,6";
    $resultPostedPosts = $wpdb->get_results($getPostedPosts);

    return $resultPostedPosts;
}