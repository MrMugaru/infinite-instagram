<script>
    function countDownTimer(time, div) {
        var countDownDate = new Date(time * 1000).getTime();
        var x = setInterval(function () {
            var now = new Date().getTime();
            var distance = countDownDate - now;
            var days = Math.floor(distance / (1000 * 60 * 60 * 24));
            var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
            var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
            var seconds = Math.floor((distance % (1000 * 60)) / 1000);
            document.getElementById(div).innerHTML = days + "d " + hours + ":"
                + minutes + ":" + seconds;
            if (distance < 0) {
                clearInterval(x);
                document.getElementById(div).innerHTML = "POSTED";
            }
        }, 1000);
    }
</script>
<div id="infinite-instagram-header">
    <img src="<?= INFINITE_INSTAGRAM_IMAGES ?>/logo-no-back.png" width="80px"/> Infinite Instagram planned posts
</div>
<table class="widefat fixed" cellspacing="0">
    <thead>
    <tr>
        <th id="columnname" class="manage-column column-columnname" scope="col">Image</th>
        <th id="columnname" class="manage-column column-columnname" scope="col">Caption text</th>
        <th id="columnname" class="manage-column column-columnname" scope="col">Time to post</th>
        <th id="columnname" class="manage-column column-columnname" scope="col">Will be posted in</th>
    </tr>
    </thead>
    <?php
    $postList = getListPlannedPosts();
    foreach ($postList as $post) { ?>
        <tr>
            <td>
                <div class="thumbnail">
                    <?= '<img class="instagramImg" src="' . wp_upload_dir()['baseurl'] . '/instagram-uploads/' . $post->image . '" />'; ?>
                </div>
            </td>
            <td><?= $post->caption; ?></td>
            <td><?= $post->time; ?></td>
            <td id="timer<?= $post->id; ?>">
                <script>
                    countDownTimer(<?= strtotime($post->time); ?>, "timer<?=$post->id?>");
                </script>
            </td>
        </tr>
    <?php } ?>
</table>