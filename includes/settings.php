<div id="infinite-instagram-header">
    <img src="<?= INFINITE_INSTAGRAM_IMAGES ?>/logo-no-back.png" width="80px"/> Infinite Instagram settings
</div>
<div class="settings-container">
    <div class="settings-left">
        <h2>Instagram account credentials</h2>
        <h4>Fill in to post images to your Instagram feed</h4>
        <form method="post" action="options.php">
            <?php settings_fields('infinite-instagram-settings-set-user'); ?>
            <?php do_settings_sections('infinite-instagram-settings-set-user'); ?>
            <table>
                <tr>
                    <td>
                        Instagram Username:
                    </td>
                    <td>
                        <input type="text" name="instagram_user"
                               value="<?= esc_attr(get_option('instagram_user')); ?>"/>
                    </td>
                </tr>
                <tr>
                    <td>
                        Instagram Password:
                    </td>
                    <td>
                        <input type="password" name="instagram_pass"
                               value="<?= esc_attr(get_option('instagram_pass')); ?>"/>
                    </td>
                </tr>
                <tr>
                    <td>

                    </td>
                    <td>
                        <?php submit_button('Set user details') ?>
                    </td>
                </tr>
            </table>
        </form>
    </div>
    <div class="settings-right">
        <h2>How to set-up the plugin</h2>
        Do you want the plugin to post by itself? <br>
        You need to set up a cronjob then. <br>
        <br>
        Copy the piece of code down here and paste this in the cronjob set-up
        <br>
        page of your hosting provider, set it to run every minute.
        <br>
        <input type="text" style="width:400px;" class="txtCronjob" disabled="disabled"/>
        <br>
        <br>
        If you are not using this, you will need visitors on your website before <br>
        the plugin is posting to Instagram.
    </div>
</div>
<!--<h2>Instagram Debug Mail</h2>-->
<!--<h4>Fill in to receive an e-mail after the plugin posted an image</h4>-->
<!--<form method="post" action="options.php">-->
<!--    --><?php //settings_fields('infinite-instagram-settings-set-debug-mail'); ?>
<!--    --><?php //do_settings_sections('infinite-instagram-settings-set-debug-mail'); ?>
<!--    <table>-->
<!--        <tr>-->
<!--            <td>-->
<!--                Debug e-mail address:-->
<!--            </td>-->
<!--            <td>-->
<!--                <input type="text" name="instagram_e-mail" value="-->
<? //= esc_attr(get_option('instagram_e-mail')); ?><!--"/>-->
<!--            </td>-->
<!--        </tr>-->
<!--        <tr>-->
<!--            <td>-->
<!---->
<!--            </td>-->
<!--            <td>-->
<!--                --><?php //submit_button('Set user details') ?>
<!--            </td>-->
<!--        </tr>-->
<!--    </table>-->
<!--</form>-->