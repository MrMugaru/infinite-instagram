<?php
set_time_limit(0);
date_default_timezone_set('Europe/Amsterdam');

add_filter('cron_schedules', 'infinite_instagram_add_cron_recurrence_interval');
add_action('three_minute_action_hook', 'infiniteInstagramPostImage');
wp_schedule_event(time(), 'every_sixty_seconds', 'three_minute_action_hook');

function infinite_instagram_add_cron_recurrence_interval($schedules)
{
    $schedules['every_sixty_seconds'] = array(
        'interval' => 60,
        'display' => __('Every 60 seconds', 'textdomain')
    );

    return $schedules;
}

function mailPostIsDone()
{
    $to = get_option('instagram_e-mail');
    $subject = "Infinite-Instagram - I've just posted";
    $message = "Hi Master, I've just posted a photo to your instagram account" . get_option('instagram_user');

    wp_mail($to, $subject, $message);
}

require 'instagramAPI/vendor/autoload.php';
// include 'http://localhost/wish-plugin/Applications/XAMPP/xamppfiles/htdocs/wish-plugin/wp-content/plugins/infinite-instagram/vendor/autoload.php';

function infiniteInstagramPostImage()
{
    global $wpdb;
/////// CONFIG ///////
    $username = get_option('instagram_user');
    $password = get_option('instagram_pass');
    $debug = false;
    $truncatedDebug = false;
//////////////////////
    $getImages = "SELECT * FROM `" . $wpdb->prefix . "infinite_instagram_planned_posts` WHERE isPosted = '0' ORDER BY id DESC";
    $resultPost = $wpdb->get_results($getImages);

    $timePost = strtotime($resultPost[0]->time);
    $timeNow = strtotime(date('Y-m-d H:i:s'));
    if ($timePost <= $timeNow) {
        $photoFilename = wp_upload_dir()['basedir'] . '/instagram-uploads/' . $resultPost[0]->image;
        $captionText = $resultPost[0]->caption;

        $ig = new \InstagramAPI\Instagram($debug, $truncatedDebug);

        try {
            $ig->setUser($username, $password);
            $ig->login();
        } catch (\Exception $e) {
            echo 'Something went wrong: ' . $e->getMessage() . "\n";
            exit(0);
        }

        try {
            $ig->uploadTimelinePhoto($photoFilename, ['caption' => $captionText]);
        } catch (\Exception $e) {
            echo 'Something went wrong: ' . $e->getMessage() . "\n";
        }
        $isPosted = "UPDATE `" . $wpdb->prefix . "infinite_instagram_planned_posts` SET isPosted = '1' WHERE id = " . $resultPost[0]->id;
        $wpdb->query($isPosted);
        if (!empty(get_option('instagram_user'))) {
//            mailPostIsDone();
        }
    }
}
