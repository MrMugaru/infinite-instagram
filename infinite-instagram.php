<?php
/**
 * Plugin Name: Infinite Instagram
 * Plugin URI: http://basig.nl
 * Description: A plugin that let you plan instagram posts and post them automatically.
 * Version: 0.2
 * Author: Bas Meyer & Richard de Feiter
 * Author URI: http://basig.nl
 * License: GPL2
 */

include(plugin_dir_path(__FILE__) . 'instagramAPI/vendor/autoload.php');
include(plugin_dir_path(__FILE__) . 'includes/classes/instagram-actions.class.php');  // Include classes
include(plugin_dir_path(__FILE__) . 'functions.php');

register_activation_hook(__FILE__, 'infiniteInstagramPlannedPosts'); // To create DB Column

function register_infinite_instagram()
{

}

function infinite_instagram_menu()
{
    add_menu_page(
        'Infinite instagram Options',
        'Infinite Instagram',
        'administrator',
        'infinite-instagram-plugin-settings',
        'infinite_instagram_home_page',
        INFINITE_INSTAGRAM_IMAGES . 'icon-16x16.png');
    if (!empty(get_option('instagram_user')) && !empty(get_option('instagram_pass'))) {
        add_submenu_page('infinite-instagram-plugin-settings', 'Submenu Page Title', 'Plan new posts', 'manage_options', __FILE__ . '/instagram-new-posts', 'infinite_instagram_new_posts');
        add_submenu_page('infinite-instagram-plugin-settings', 'Submenu Page Title', 'Posts posted', 'manage_options', __FILE__ . '/instagram-posted-posts', 'infinite_instagram_posted_posts');
        add_submenu_page('infinite-instagram-plugin-settings', 'Submenu Page Title', 'Post queue', 'manage_options', __FILE__ . '/instagram-posts-queue', 'infinite_instagram_posts_queue');
    }
    add_submenu_page('infinite-instagram-plugin-settings', 'Submenu Page Title', 'Settings', 'manage_options', __FILE__ . '/instagram-settings', 'infinite_instagram_settings_page');
}

function infinite_instagram_home_page()
{
    include dirname(__FILE__) . '/includes/home.php';
}

function infinite_instagram_new_posts()
{
    include dirname(__FILE__) . '/includes/plan-post.php';
}

function infinite_instagram_posted_posts()
{
    include dirname(__FILE__) . '/includes/posted-posts.php';
}

function infinite_instagram_posts_queue()
{
    include dirname(__FILE__) . '/includes/planned-posts.php';
}

function infinite_instagram_settings_page()
{
    include dirname(__FILE__) . '/includes/settings.php';
}

function infinite_instagram_css_and_js()
{
    wp_register_style('infinite_instagram_css_and_js', plugins_url('assets/style/style.css', __FILE__));
    wp_enqueue_style('infinite_instagram_css_and_js');
    wp_register_script('infinite_instagram_css_and_js', plugins_url('assets/js/infinite-instagram.js', __FILE__));
    wp_enqueue_script('infinite_instagram_css_and_js');
}

/// TO CREATE DB COLUMNS
function infiniteInstagramPlannedPosts()
{
    global $wpdb;
    $charset_collate = $wpdb->get_charset_collate();
    $table_name = $wpdb->prefix . 'infinite_instagram_planned_posts';

    $sql = "CREATE TABLE $table_name (
		id mediumint(9) NOT NULL AUTO_INCREMENT,
		image varchar(255) NOT NULL,
		caption text(999) NOT NULL,
		isPosted smallint(5) NOT NULL,
		time datetime DEFAULT '0000-00-00 00:00:00' NOT NULL,
		added datetime DEFAULT '0000-00-00 00:00:00' NOT NULL,
		UNIQUE KEY id (id)
	) $charset_collate;";

    require_once(ABSPATH . 'wp-admin/includes/upgrade.php');
    dbDelta($sql);
}

function infinite_instagram_set_user()
{
    register_setting('infinite-instagram-settings-set-user', 'instagram_user');
    register_setting('infinite-instagram-settings-set-user', 'instagram_pass');
}

function infinite_instagram_set_mail()
{
    register_setting('infinite-instagram-settings-set-debug-mail', 'instagram_e-mail');
}

define('INFINITE_INSTAGRAM_IMAGES', plugin_dir_url(__FILE__) . '/assets/img/');
add_action('admin_init', 'infinite_instagram_css_and_js');
add_action('wp_enqueue_scripts', 'infinite_instagram_css_and_js');
add_action('admin_init', 'infinite_instagram_set_user');
add_action('admin_init', 'infinite_instagram_set_mail');
add_action('admin_menu', 'infinite_instagram_menu');

