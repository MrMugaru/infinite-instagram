# README #

Wordpress Plugin that let you plan Instagram posts and posts them automatically at that time! 

### How do I get set up? ###

1. Upload & install the plugin to your WordPress website. 
2. Activate the plugin in your WordPress plugin list.
3. Go to the settings of the Infinite-Instagram Plugin.  
4. Fill in your Instagram Credentials. 
5. You will now be able to plan posts. 

Want the plugin to be 100% automatic (posting)
1. Go to the settings page of the plugin. 
2. Copy the cronjob thats in the textbox. 
3. Login to your hosting and set the cronjob to run every minute. 
4. Your posts will now be posted automatically. 

If you do not set the Cronjob, people have to visit your website before the plugin is able to post. So it won't be 100% automatic then. 

### Short FAQ ###
Q. Probably because you have used the wrong login credentials. 
A. Test them by logging in to your Instagram account on your mobile phone or the Instagram website. 



### Who do I talk to? ###

* Contact us at Envato Market by sending a comment, we will get back to you as soon as possible.
 
