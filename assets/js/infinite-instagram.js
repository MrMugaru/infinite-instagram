function readURL(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();
        reader.onload = function (e) {
            jQuery('#instagramImage').attr('src', e.target.result);
        }
        reader.readAsDataURL(input.files[0]);
    }
}

function getURL() {
    return location.protocol + '//' + location.host
}

function setCronjobUrl() {
    return jQuery(".txtCronjob").val("/usr/bin/wget -O /dev/null " + getURL() + "/wp-cron.php");
}

function parseUpdateNews() {
    jQuery.getJSON('http://basig.nl/api/updatenews/update.json', function (data) {
        jQuery.each(data.updates, function (i, val) {
            var item = data.updates[i];
            jQuery('.update-items').append('<tr><td>' + item.date + '</td><td><strong>' + item.title + '</strong></td><td>' + item.content + '</td></tr>');
        });
    });
}

function parseNews() {
    jQuery.getJSON('http://basig.nl/api/news/news.json', function (data) {
        jQuery.each(data.news, function (i, val) {
            var item = data.news[i];
            jQuery('.news-items').append('<tr><td>' + item.date + '</td><td><strong>' + item.title + '</strong></td><td>' + item.content + '</td></tr>');
        });
    });
}

jQuery(document).ready(function () {
    parseUpdateNews();
    parseNews();
    setCronjobUrl();

    jQuery(".btnPlanPost").prop('disabled', true);

    jQuery("#uploadInstagramImage").change(function () {
        readURL(this);
    });

    var _URL = window.URL || window.webkitURL;
    jQuery("#uploadInstagramImage").change(function (e) {
        var file, img;
        if ((file = this.files[0])) {
            img = new Image();
            img.onload = function () {
                var aspectRatio = (this.width / this.height);
                var validExtensions = ['jpg', 'jpeg']; //array of valid extensions
                var fileName = file.name;
                var fileNameExt = fileName.substr(fileName.lastIndexOf('.') + 1);

                if (jQuery.inArray(fileNameExt, validExtensions) == -1) {
                    jQuery(".btnPlanPost").prop('disabled', true);
                    jQuery(".errorMessage td").show().html("Only files with the jpg extension can be uploaded.");
                } else if (this.width > 1080 || this.height > 1350) { // 1080x1350  == MAX...
                    jQuery(".btnPlanPost").prop('disabled', true);
                    jQuery(".errorMessage td").show().html("The image is way to large.. it can't be uploaded. Max is: W:1080 - H:1350.");
                } else if (aspectRatio.toFixed(2) < 0.80 || aspectRatio.toFixed(2) > 1.91) {
                    jQuery(".btnPlanPost").prop('disabled', true);
                    jQuery(".errorMessage td").show().html("Your image aspect ratio should be between 0.80 and 1.91, the image you uploaded has an aspect ratio of: " + aspectRatio.toFixed(2) + ".");
                } else {
                    jQuery(".btnPlanPost").prop('disabled', false);
                    jQuery(".errorMessage td").hide();
                }
            };
            img.src = _URL.createObjectURL(file);
        }
    });

    jQuery('.caption').keyup(function () {
        var count = jQuery(this).val().match(/#/g);
        if (count < 1 || count === null) {
            jQuery(".btnPlanPost").prop('disabled', false);
            jQuery(".errorMessage td").hide();
            jQuery('.hashtagCount').html("0");
        } else if (count.length >= 20 && count.length < 29) {
            jQuery(".btnPlanPost").prop('disabled', false);
            jQuery(".errorMessage td").hide();
            jQuery('.hashtagCount').html(count.length).css({'color': 'orange'});
            jQuery('.hashtagCountStd').css({'color': 'orange'});
        } else if (count.length > 28 && count.length < 31) {
            jQuery(".btnPlanPost").prop('disabled', false);
            jQuery(".errorMessage td").hide();
            jQuery('.hashtagCount').html(count.length).css({'color': 'red'});
            jQuery('.hashtagCountStd').css({'color': 'red'});
        } else if (count.length > 30) {
            jQuery(".btnPlanPost").prop('disabled', true);
            jQuery(".errorMessage td").show().html("The max allowed hashtags are 30. You are using more, so you won't be able to post");
        } else {
            jQuery(".btnPlanPost").prop('disabled', false);
            jQuery(".errorMessage td").hide();
            jQuery('.hashtagCount').html(count.length).css({'color': 'black'});
        }
    });

    jQuery(".txtPostTime").change(function () {
        var image = jQuery("#uploadInstagramImage").val();
        var caption = jQuery(".caption").val();
        if (image !== '' && caption !== '') {
            jQuery(".successMessage td").show().html("You can now plan the post.");
            jQuery(".btnPlanPost").prop('disabled', false);
        } else {
            jQuery(".successMessage td").hide();
            jQuery(".btnPlanPost").prop('disabled', true);
        }
    });
});


